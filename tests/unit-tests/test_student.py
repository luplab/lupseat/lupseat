import unittest
from lupseat.student import Student


class StudentMethods(unittest.TestCase):
    def test_get_spec_students(self):
        stdts = {
            1: Student({'lastname': 'Fname', 'firstname': 'Lname', 'sid': '1',
                        'specifiers': ''}),
            2: Student({'lastname': 'Fname', 'firstname': 'Lname', 'sid': '2',
                        'specifiers': 'l'}),
            3: Student({'lastname': 'Fname', 'firstname': 'Lname', 'sid': '3',
                        'specifiers': 'a'}),
            4: Student({'lastname': 'Fname', 'firstname': 'Lname', 'sid': '4',
                        'specifiers': 'la'}),
            5: Student({'lastname': 'Fname', 'firstname': 'Lname', 'sid': '5',
                        'specifiers': ''}),
        }

        stdt_l = Student.get_spec_students(stdts, left_hand=False,
                                           ada_seat=False)
        self.assertEqual(len(stdt_l), 2)
        self.assertTrue(1 in stdt_l)
        self.assertTrue(5 in stdt_l)

        stdt_l = Student.get_spec_students(stdts, left_hand=True,
                                           ada_seat=False)
        self.assertEqual(len(stdt_l), 1)
        self.assertTrue(2 in stdt_l)

        stdt_l = Student.get_spec_students(stdts, left_hand=False,
                                           ada_seat=True)
        self.assertEqual(len(stdt_l), 1)
        self.assertTrue(3 in stdt_l)

        stdt_l = Student.get_spec_students(stdts, left_hand=True,
                                           ada_seat=True)
        self.assertEqual(len(stdt_l), 1)
        self.assertTrue(4 in stdt_l)
