import setuptools

with open("README.md", "r") as fh:
    long_description  = fh.read()

with open("VERSION", "r") as fh:
    version = fh.read()

setuptools.setup(
        name='LupSeat',
        version=version,
        author="LupLab",
        author_email="jporquet@ucdavis.edu",
        description="Automatically assigns seats to students in a smart way.",
        license='GNU GPL-3',
        url='https://gitlab.com/luplab/lupseat/lupseat',
        long_description=long_description,
        long_description_content_type="text/markdown",
        entry_points={
            "gui_scripts": [
                "lupseat = lupseat.__main__:main"
            ]
        },
        packages=setuptools.find_packages(),
        package_data={'': ['*']},
        include_package_data=True,
        install_requires=[
            'Pillow>=8.0.1',
            'fpdf>=1.7.2',
            'papersize>=1.0.1',
        ],
)
