run: ./lupseat/__main__.py
	python -m lupseat

init: requirements.txt
	pip install -r requirements.txt

pipbuild: setup.py
	python3 setup.py bdist_wheel
	python3 -m twine upload dist/LupSeat-$(cat VERSION)*

clean:
	rm -rf chart.*
	rm -rf *.jpg
	rm -rf build
	rm -rf dist
	rm -rf LupSeat.egg-info
