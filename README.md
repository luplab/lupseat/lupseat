# LupSeat

LupSeat randomly assigns seats to students in a smart and automatic away to
prevent cheating during exams.

## Install

### Binaries

Download the [release](https://gitlab.com/luplab/lupseat/lupseat/-/releases)
suited for your operating system (Linux/macOS/Windows) and double click or run
from the command line.

### From source

```console
$ git clone https://gitlab.com/luplab/lupseat/lupseat.git
$ cd lupseat
$ virtualenv
(env) $ pip install -r requirements.txt
(env) $ python -m lupseat --help
```

## Usage

LupSeat takes two mandatory input files, a student roster and a classroom
description, and generates an randomized seating chart.

### Required arguments

- `--student <roster.csv>`

    A CSV-formatted file containing first name, last name, student ID number
    (SID), and optional section that can indicate if a student is left handed
    (`l`) or has accessible seating needs (`a`).

    ```csv
    Carree,Heggs,1378,l
    Ludvig,Quinane,1821,a
    Charisse,Scemp,1924,
    Hazlett,Michie,2218,
    Freedman,Norree,2251,
    Aurelia,Besantie,2794,
    ```

- `--seats <classroom.txt>`

    A YAML-formatted file containing the seating arrangement of the room where
    each line indicates a row with seats arranged left to right, from the
    speakers point of view. A comma within a line indicates an aisle splitting a
    contiguous set of seats. The specifiers section can be used to indicate
    seats that are broken (`b`), left-handed (`l`), or accessible seating (`a`).

    ```yml
    Seats:
    a[1:1],a[3:5],a[6:9]
    b[1:5],b[6:9]
    c[1:5],c[6:9]
    d[1:5],d[6:9]
    e[1:5],e[6:9]

    Specifiers:
    b:c[2]
    a:a[1]
    l:a[6],b[6],c[6],d[6],e[6]
    ```

### Optional arguments

- `--out <chart.csv>`

    Name of CSV-formatted output file containing the seating chart.

- `--fmt <format>`

    This format string specifies how students are represented in the output
    file. The students' first name, last name, and SID are accessible via
    variables `{fname}`, `{lname}`, and `{sid}` respectively. Each of these
    variables can be sliced using the pipe operator.

    For example, `--fmt "{fname|0}{lname|0}-{sid|0,2}"` will identify students
    by their initials and the first two digits of their SID.

    By default, students are identified by their SID.

- `--sort_by <criterion>`
    * `seat`: sort seating chart by seat number. *(Default)* default))
    * `fname`, `lname`, or `sid`: sort seating chart by the students' first
      name, last name, or SID respectively.

- `--g_chart <chart.pdf>`

    Name of printable PDF output file containing the seating chart.

- `--g_room <classroom.jpg>`

    Name of printable JPEG output file representing the classroom graphically.

- `--g_chart_size <size>` and `--g_room_size <size>`

    Size of each graphical output. The available sizes are those supported by
    the [papersize](http://papersize.readthedocs.io/) package. You can also add
    the extra keyword `flip` to switch between landscape and portrait.

    The default is `"a4 flip"` for an A4 portrait output.

- `--seed <seed>`

    Specify seed for the random number generator used to randomly assign
    students to seats. If none is specified, seed is randomly generated.

- `--algorithm <type>`
    * `ConsecDivice`: a top-down approach where the room is consecutively
      divided until all the required empty seats have allocated. *(Default)*
    * `ChunkIncrease`: a bottom-up approach where the chunk size (i.e., the
      number of students sitting contiguously) slowly increases until all
      students can fit in the room.
    * `RandomAssign`: random assignment of seats *(Not recommended)*

- `--partner <partners.csv>`

    A CSV-formatted file where each line indicates students that have worked
    together as partners. Students on each line are indicated by their SID and
    are separated by a comma.

    ```csv
    2218,2251
    2794,1924
    ```

- `--gui`

    Run GUI mode. Default if no command-line options are provided.

- `--font <font>`

    The name of the TrueType font used for the graphical output files. Default
    is `Roboto-Light`.

    The name of the font must be exact. If the specified font cannot be found it
    will attempt to use other similar, commonly found fonts available on
    different systems (i.e., `ARIALN`, `arial`, `LiberationSansNarrow-Regular`,
    `SFCompact`).

    If no fonts are found it will use the built-in default font from Pillow, but
    in that case the font size cannot be controlled and will probably in a
    suboptimal graphical output.

- `--nochart`, `--nopdf`, `--noimage`

    Deactivate the generation of the CSV-formatted seating chart, the PDF
    seating chart, and the JPEG classroom representation.

#### Development-only arguments

- `--eval`

    Produce evaluation score for the seating algorithm, that is the average
    number of students sitting next to each other. Lower score is better.

- `--nosave`

    Disable saving output files. This option is primarily used for automated
    evaluations.

## License

LupSeat is an original idea by Joël Porquet-Lupine at the
[LupLab](https://luplab.cs.ucdavis.edu/) and is licensed under the
[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html).

Contributors include:

- Hiroya Gojo, 2020-2021 (wrote the first functional version :clap:)
- Philip Breault, 2021-present
