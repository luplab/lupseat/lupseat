# SPDX-License-Identifier: GPL-3.0
#
# Copyright (c) 2020-2022 LupLab et al.
#

from lupseat.seat import Seat, SeatGroups


def chr_to_int(char):
    """Convert row char to row number
    Args:
        char : ('a', 'b')

    Returns:
        int:  row number (1 to max_row)
    """
    return ord(char.lower()) - ord('a') + 1


def int_to_chr(num):
    """Convert row number to row char
    Args:
        num (int): 0 to max_row-1

    Returns:
        int: row char ('a', 'b')
    """
    return chr(ord('a') + num)


def seat_inds(seat):
    """Get seat indices"""
    if not seat[0].isalpha():
        raise Exception(f"Seat {seat} not formatted correctly")
    if not seat[1:].isnumeric():
        raise Exception(f"Seat {seat} not formatted correctly")

    cur_row = chr_to_int(seat[0])
    cur_col = int(seat[1:])
    return cur_row, cur_col


def range_seat_inds(seats):
    """ Get seat indices, handles ranges as well"""
    name, indices = seats.split('[')
    name = chr_to_int(name)
    indices = indices.strip(']')

    if ':' in seats:
        seat_pos1 = name, int(indices.split(':')[0])
        seat_pos2 = name, int(indices.split(':')[1])
        if seat_pos1[0] != seat_pos2[0] or seat_pos1[1] >= seat_pos2[1]:
            raise Exception(f"Range {seats} is malformed")

        list_of_seats = []
        cur_row = seat_pos1[0]
        for cur_col in range(int(seat_pos1[1]), int(seat_pos2[1]) + 1):
            list_of_seats.append((cur_row, cur_col))

        return list_of_seats
    return [(name, int(indices))]


def process_str(raw_str):
    """Removes all whitespace and makes lowercase.
    Removes any trailing colons."""
    if raw_str == '':
        return ''
    if raw_str[-1] == ':':
        raw_str = raw_str[:-1]
    return "".join(raw_str.lower().split())


def get_col_inds(col_range):
    """Gets column indices in iterator form"""
    if ':' in col_range:
        beg_seat = col_range.split(':')[0]
        end_seat = col_range.split(':')[1]

        beg = int(beg_seat.split('[')[1])
        end = int(end_seat.strip(']'))
        return range(beg, end + 1)
    if str.isdigit(col_range[1:]):
        return range(int(col_range[1:]), int(col_range[1:]) + 1)

    raise Exception(f"Unknown col range format: {col_range}")


class Room:
    """Contains grid representation of room containing seat instances"""

    def __init__(self):
        self.max_row = 1
        self.max_col = 1
        self.seats = []
        # Specifies where the row is discontinuous (can be considered new chunk)
        self.row_breaks = []

    def __str__(self):
        output = ''
        for rows in self.seats:
            for seat in rows:
                if seat is None:
                    output += "{0: <3}".format('n')
                else:
                    output += "{0: <3}".format(seat.sid)
                output += ' '
            output += '\n'
        return output

    def __repr__(self):
        return str(self)

    @classmethod
    def create_room(cls, filepath):
        """Instantiates room class with correct size and seats.
        Args:
            filepath (str): path to yaml file describing room layout.

        Returns:
            Room: Room instance with correct size, seats & seat qualities
            specified.
        """
        with open(filepath, 'r') as file:
            f_raw = file.read()

        room = cls()
        room._update_size(f_raw)
        room._add_seats(f_raw)
        room._add_seat_specifiers(f_raw)
        return room

    def _update_size(self, f_raw):
        """Gets dimensions of room (during instantiation)"""
        seat_flag = False
        for line in f_raw.splitlines():
            line = process_str(line)
            if line in "seats" or line in "seat":
                seat_flag = True
            elif line in "specifiers" or line in "specifier":
                seat_flag = False
            elif seat_flag:
                # Skip if empty line
                if line == "":
                    continue

                # Otherwise check if seat range is new max
                for seat_range in line.split(','):
                    # Skip if empty range
                    if seat_range == '':
                        continue

                    # Append row name to index
                    row_idc = seat_range.split('[')[0]
                    row_idc += seat_range.split(':')[-1].strip(']')
                    cur_row, cur_col = seat_inds(row_idc)

                    if cur_row > self.max_row:
                        self.max_row = cur_row
                    if cur_col > self.max_col:
                        self.max_col = cur_col

    def _add_seats(self, f_raw):
        """Adds seats to room (during instantiation)"""
        # Row major
        for _ in range(self.max_row):
            self.row_breaks.append([])
            self.seats.append([None] * self.max_col)

        seat_flag = False
        infer_row_num = 0
        for line in f_raw.splitlines():
            line = process_str(line)
            if line in "seats" or line in "seat":
                seat_flag = True
            elif line in "specifiers" or line in "specifier":
                seat_flag = False
            elif seat_flag:
                # Skip if empty line
                if line == "":
                    continue

                # Update inference
                infer_row_num += 1

                # Add seats per each range
                for row_range in line.split(','):
                    # Skip if empty range
                    if row_range == '':
                        # Infer row from previous rows
                        self.row_breaks[infer_row_num - 1].append(0)
                        continue

                    # Add seat
                    cur_row = chr_to_int(row_range[0])
                    cur_col = 1
                    for cur_col in get_col_inds(row_range):
                        self.seats[cur_row - 1][cur_col - 1] = \
                            Seat(int_to_chr(cur_row - 1), cur_col)

                    # Store row break
                    self.row_breaks[cur_row - 1].append(cur_col - 1)

    def _add_seat_specifiers(self, f_raw):
        """Adds seat features to each seat (during instantiation)"""
        spec_flag = False
        for line in f_raw.splitlines():
            line = process_str(line)
            if line in "specifiers" or line in "specifier":
                spec_flag = True
            elif line in "seats" or line in "seat":
                spec_flag = False
            elif spec_flag:
                # Skip if empty line
                if line == "":
                    continue

                # Get flag
                flag = line.split(':')[0]
                if flag not in 'bla' and len(flag) != 1:
                    raise Exception(f"Flag must be B, L, or A: {line}")

                # Get list of seats to apply flag to
                seat_inds = list(
                    map(range_seat_inds, line[2:].split(',')))
                flat_seat_inds = [item for sublist in seat_inds for item in
                                  sublist]

                for cur_row, cur_col in flat_seat_inds:
                    if self.seats[cur_row - 1][cur_col - 1] is None:
                        raise Exception(
                            f"Adding quality to seat that doesn't exist {line}")

                    # Apply flag to seats
                    if flag == 'b':
                        self.seats[cur_row - 1][cur_col - 1].broken = True
                    if flag == 'l':
                        self.seats[cur_row - 1][cur_col - 1].left_handed = True
                    if flag == 'a':
                        self.seats[cur_row - 1][cur_col - 1].ada_seat = True

    def add_student(self, indices, sid):
        self.seats[indices[0]][indices[1]].sid = sid

    def check_neighbors_safe(self, indices, partner_sids):
        # Check to the left
        if indices[1] != 0:
            if self.seats[indices[0]][indices[1] - 1] is None:
                pass
            elif self.seats[indices[0]][indices[1] - 1].sid in partner_sids:
                return False

        # Check to the right
        if indices[1] != len(self.seats[0]) - 1:
            if self.seats[indices[0]][indices[1] + 1] is None:
                pass
            elif self.seats[indices[0]][indices[1] + 1].sid in partner_sids:
                return False
        return True

    def set_enable(self, indices, val):
        self.seats[indices[0]][indices[1]].enable = val

    def get_spec_seats(self, left_hand=False, ada_seat=False):
        """Gets specified seats or seats with more specificity
        e.g. If getting left_handed seats, will look for both ada_seat
        and non_ada_seats

        Args:
            left_hand (bool): flag for whether to search for left or right
            handed seats
            ada_seat (bool): flag for whether to search for accessible seating

        Returns:
            List of tuples: List of indices corresponding to seat locations.
        """
        match_seats = []

        for row in range(self.max_row):
            for col in range(self.max_col):
                # Ignore if seat not there
                if self.seats[row][col] is None:
                    continue

                # Ignore broken seats
                if self.seats[row][col].broken:
                    continue

                # Ignore disabled seats
                if not self.seats[row][col].enable:
                    continue

                # Ignore if seat is taken
                if self.seats[row][col].sid != -1:
                    continue

                # Add seats based on condition
                if left_hand and ada_seat:
                    if self.seats[row][col].left_handed and \
                            self.seats[row][col].ada_seat:
                        match_seats.append((row, col))
                elif left_hand:
                    if self.seats[row][col].left_handed:
                        match_seats.append((row, col))
                elif ada_seat:
                    if self.seats[row][col].ada_seat:
                        match_seats.append((row, col))
                else:
                    match_seats.append((row, col))

        # Ensure that seats are sorted by row
        match_seats.sort()

        return match_seats

    def split_to_chunks(self):
        """Splits seats into SeatGroup chunks if they are continuous and
         not broken.
        Returns:
            List[SeatGroups]: List of SeatGroups representing preliminary chunk.
        """
        # Populate chunks assuming infinite max_chunk_size
        chunks = []

        is_chunk = False
        chunk_begin = None

        for row in range(self.max_row):
            for col in range(self.max_col):
                # Start new chunk
                if not is_chunk and self.seats[row][col] is not None and not \
                        self.seats[row][col].broken:
                    is_chunk = True
                    chunk_begin = (row, col)
                # End current chunk
                elif is_chunk and (self.seats[row][col] is None or
                                   self.seats[row][col].broken):
                    is_chunk = False
                    chunk_end = (row, col - 1)
                    chunks.append(SeatGroups(chunk_begin, chunk_end))

                # End current chunk due to row break
                if is_chunk and col in self.row_breaks[row]:
                    is_chunk = False
                    chunk_end = (row, col)
                    chunks.append(SeatGroups(chunk_begin, chunk_end))

            if is_chunk:
                is_chunk = False
                chunk_end = (row, col - 1)
                chunks.append(SeatGroups(chunk_begin, chunk_end))

        return chunks
