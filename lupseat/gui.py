# SPDX-License-Identifier: GPL-3.0
#
# Copyright (c) 2020-2022 LupLab et al.
#
import os
import sys

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QGroupBox, QHBoxLayout, \
    QPushButton, QLineEdit, QLabel, QComboBox, QFormLayout, QCheckBox, \
    QFileDialog, QApplication


class MainWindow(QWidget):
    def __init__(self, args):
        super(MainWindow, self).__init__()
        # Set and initialize title
        self.title = 'LupSeat - Smart Seat Assignment Generator'
        self.setWindowTitle(self.title)

        layout = QVBoxLayout()

        # Initialize groupboxes
        layout.addWidget(self.input_box_ui())
        layout.addWidget(self.settings_box_ui())
        layout.addWidget(self.output_box_ui(args))

        self.output_dir = None
        self.student_file = None
        self.room_file = None
        self.partner_file = None
        self.csv_chart_name = None
        self.graphic_chart_name = None
        self.graphic_chart_size = None
        self.graphic_room_name = None
        self.graphic_room_size = None
        self.format_string = None
        self.seed = None
        self.sort_by = None
        self.algorithm = None
        self.create_chart = None
        self.create_image = None
        self.create_pdf = None

        self.setLayout(layout)

    def input_box_ui(self):
        group_box = QGroupBox('Input')
        vert_box = QVBoxLayout()

        layout = QHBoxLayout()
        self.student_button = QPushButton('Select Student File')
        self.student_button_ui()
        layout.addWidget(self.student_button)

        self.student_text = QLineEdit()
        self.student_text.setReadOnly(True)
        layout.addWidget(self.student_text)
        self.student_text.textChanged.connect(self.student_line_event)

        layout2 = QHBoxLayout()
        self.room_button = QPushButton('Select Room File')
        self.room_button_ui()
        layout2.addWidget(self.room_button)

        self.room_text = QLineEdit(self)
        self.room_text.setReadOnly(True)
        layout2.addWidget(self.room_text)
        self.room_text.textChanged.connect(self.room_text_event)

        vert_box.addLayout(layout)
        vert_box.addLayout(layout2)
        group_box.setLayout(vert_box)
        return group_box

    def settings_box_ui(self):
        group_box = QGroupBox('Settings')
        vert_box = QVBoxLayout()

        first_row = QHBoxLayout()
        self.partner_button = QPushButton('Select Partner File')
        self.partner_button_ui()
        first_row.addWidget(self.partner_button)

        self.partner_text = QLineEdit(self)
        self.partner_text.setReadOnly(True)
        first_row.addWidget(self.partner_text)

        second_row = QHBoxLayout()
        self.format_line = QLineEdit('{sid}')
        self.format_string = '{sid}'
        second_row.addWidget(QLabel('Format String'))
        second_row.addWidget(self.format_line)
        self.format_line.textChanged.connect(self.format_line_event)

        second_row.addWidget(QLabel("Sort By"))

        self.sort_box = QComboBox()
        self.sort_box.addItems(['sid', 'fname', 'lname', 'seat'])
        second_row.addWidget(self.sort_box)
        self.sort_box.currentTextChanged.connect(self.sort_by_event)

        third_row = QHBoxLayout()
        third_row.addWidget(QLabel('Seed'))
        self.seed_line = QLineEdit('-1')
        third_row.addWidget(self.seed_line)
        self.seed_line.textChanged.connect(self.seed_line_event)

        third_row.addWidget(QLabel("Algorithm"))
        self.algo_box = QComboBox()
        self.algo_box.addItems(['consecdivide', 'chunkincrease',
                                'randomassign'])
        third_row.addWidget(self.algo_box)
        self.algo_box.currentTextChanged.connect(self.algo_event)

        vert_box.addLayout(first_row)
        vert_box.addLayout(second_row)
        vert_box.addLayout(third_row)
        group_box.setLayout(vert_box)
        return group_box

    def output_box_ui(self, args):
        group_box = QGroupBox('Output')

        layout = QFormLayout()

        save_boxes = QHBoxLayout()
        self.chart_box = QCheckBox('Chart')
        self.chart_box.stateChanged.connect(self.chart_click)
        save_boxes.addWidget(self.chart_box)

        self.image_box = QCheckBox('Image')
        self.image_box.stateChanged.connect(self.image_click)
        save_boxes.addWidget(self.image_box)

        self.pdf_box = QCheckBox('PDF')
        self.pdf_box.stateChanged.connect(self.pdf_click)
        save_boxes.addWidget(self.pdf_box)
        layout.addRow(QLabel('Create'), save_boxes)

        self.directory_button = QPushButton('Output Directory')
        self.directory_button_ui()

        self.directory_text = QLineEdit(self)
        self.directory_text.setReadOnly(True)
        self.directory_text.textChanged.connect(self.directory_line_event)
        layout.addRow(self.directory_button, self.directory_text)

        self.save_button = QPushButton('Save')
        layout.addRow(self.save_button)
        self.save_button.clicked.connect(lambda: self.run_click(args))

        group_box.setLayout(layout)
        return group_box

    def format_line_event(self):
        self.format_string = self.format_line.text()

    def seed_line_event(self):
        self.seed = self.seed_line.text()

    def sort_by_event(self):
        self.sort_by = self.sort_box.currentText()

    def algo_event(self):
        self.algorithm = self.algo_box.currentText()

    def room_text_event(self):
        self.room_file = self.room_text.text()

    def student_line_event(self):
        self.student_file = self.student_text.text()

    def partner_line_event(self):
        self.partner_file = self.partner_text.text()

    def directory_line_event(self):
        self.output_dir = self.directory_text.text()

    def set_student_line(self, file_path):
        self.student_text.setText(file_path)
        self.student_file = file_path

    def set_room_line(self, file_path):
        self.room_text.setText(file_path)
        self.room_file = file_path

    def set_partner_line(self, file_path):
        self.partner_text.setText(file_path)
        self.partner_file = file_path

    def set_output_line(self, file_path):
        self.directory_text.setText(file_path)
        self.output_dir = file_path

    def student_button_ui(self):
        dialog_name = 'Select Student File'
        file_trim = 'Student File (*.csv)'
        self.student_button.clicked.connect(
            lambda: self.file_dialog(file_trim, dialog_name, True))

    def room_button_ui(self):
        dialog_name = 'Select Room File'
        file_trim = 'Room File (*.txt)'
        self.room_button.clicked.connect(
            lambda: self.file_dialog(file_trim, dialog_name, True))

    def partner_button_ui(self):
        dialog_name = 'Select Partner File'
        file_trim = 'Partner File (*.csv)'
        self.partner_button.clicked.connect(
            lambda: self.file_dialog(file_trim, dialog_name, True))

    def directory_button_ui(self):
        dialog_name = 'Select Output Directory'
        file_trim = ''
        self.directory_button.clicked.connect(
            lambda: self.file_dialog(file_trim, dialog_name, False))

    def chart_click(self):
        self.create_chart = self.chart_box.isChecked()

    def image_click(self):
        self.create_image = self.image_box.isChecked()

    def pdf_click(self):
        self.create_pdf = self.pdf_box.isChecked()

    def run_click(self, args):
        args.student = self.student_file
        args.seats = self.room_file
        args.partner = self.partner_file

        args.out = os.path.join(self.output_dir, self.csv_chart_name)
        args.g_room = os.path.join(self.output_dir, self.graphic_room_name)
        args.g_chart = os.path.join(self.output_dir, self.graphic_chart_name)

        args.g_room_size = self.graphic_room_size
        args.fmt = self.format_string
        args.seed = self.seed

        args.sort_by = self.sort_by
        args.algorithm = self.algorithm

        args.nochart = not self.create_chart
        args.noimage = not self.create_image
        args.nopdf = not self.create_pdf
        self.close()

    def file_dialog(self, file_trim, dialog_name, is_file):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        if is_file:
            file_name, _ = QFileDialog.getOpenFileName(self,
                                                       dialog_name,
                                                       "",
                                                       file_trim,
                                                       options=options)
        else:
            file_name = QFileDialog.getExistingDirectory(self, dialog_name, "",
                                                         options=options)

        if file_name:
            if dialog_name == "Select Student File":
                self.set_student_line(file_name)
            elif dialog_name == "Select Room File":
                self.set_room_line(file_name)
            elif dialog_name == "Select Partner File":
                self.set_partner_line(file_name)
            elif dialog_name == "Select Output Directory":
                self.set_output_line(file_name)

    def set_parameters(self, args):
        self.output_dir = args.out
        if args.student != 'student.csv':
            self.student_file = args.student
            self.student_text.setText(args.student)

        if args.seats != 'room.txt':
            self.room_file = args.seats
            self.room_text.setText(args.seats)

        self.partner_file = args.partner
        self.partner_text.setText(args.partner)

        self.csv_chart_name = args.out
        self.graphic_chart_name = args.g_chart
        self.graphic_chart_size = args.g_chart_size
        self.graphic_room_name = args.g_room
        self.graphic_room_size = args.g_room_size
        self.format_string = args.fmt
        self.seed = args.seed
        self.seed_line.setText(str(self.seed))

        self.sort_by = args.sort_by
        self.sort_box.setCurrentText(self.sort_by)
        self.algorithm = args.algorithm

        self.create_chart = not args.nochart
        self.chart_box.setChecked(self.create_chart)
        self.create_image = not args.noimage
        self.image_box.setChecked(self.create_image)
        self.create_pdf = not args.nopdf
        self.pdf_box.setChecked(self.create_pdf)


def start_gui(args):
    app = QApplication(sys.argv)
    ex = MainWindow(args)
    ex.set_parameters(args)
    ex.show()
    return app.exec_()


def done_gui():
    return
