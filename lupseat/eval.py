# SPDX-License-Identifier: GPL-3.0
#
# Copyright (c) 2020-2022 LupLab et al.
#
def eval_chunk_size(room):
    chunks = []
    chunk_size = 0
    for row in range(room.max_row):
        for col in range(room.max_col):
            if ((room.seats[row][col] is None or room.seats[row][col].sid == -1)
                    and chunk_size > 0):
                chunks.append(chunk_size)
                chunk_size = 0
            else:
                chunk_size += 1

        if chunk_size > 0:
            chunks.append(chunk_size)
            chunk_size = 0

    score = sum(chunks) / len(chunks)

    # Remove decimals past 2 sigfigs
    score *= 100
    score = round(score) / 100

    print(score)
