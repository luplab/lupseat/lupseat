# SPDX-License-Identifier: GPL-3.0
#
# Copyright (c) 2020-2022 LupLab et al.
#

import csv
import json


class Student:
    @staticmethod
    def header_exists(filepath):
        with open(filepath, newline='', encoding="utf8") as csvfile:
            csv_reader = csv.reader(csvfile)
            header_row = next(csv_reader)
            csvfile.seek(0)

            for i, value in enumerate(header_row):
                header_row[i] = value.lower()
            necessary_fields = ["firstname", "lastname", "sid"]
            return set(necessary_fields).issubset(header_row)

    @staticmethod
    def parse_stdt(filepath):
        """Parses student csv.
        Args:
            filepath (str): path to student csv

        Returns:
            dict{Student}: dictionary of students, identified by SID
        """
        stdts = {}
        with open(filepath, newline='', encoding="utf8") as csvfile:
            has_header = Student.header_exists(filepath)
            if has_header:
                csv_reader = csv.DictReader(csvfile)
            else:
                csv_reader = csv.reader(csvfile)
            for row in csv_reader:
                if len(row) < 3:
                    print(f"Error parsing student: {row}. Skipping.")
                    continue
                try:
                    if has_header:
                        row = {k.lower(): v for k, v in row.items()}
                        new_stdt = Student(row)
                    else:
                        # If no header assumes lastname,firstname,sid,specifiers
                        row_dict = {'firstname': row[0], 'lastname': row[1],
                                    'sid': row[2], 'specifiers': row[3]}
                        new_stdt = Student(row_dict)
                except ValueError as error:
                    print(error)
                    print(f"Error parsing student: {row}. Skipping.")
                    continue
                stdts[new_stdt.sid] = new_stdt
        return stdts

    @staticmethod
    def parse_stdt_partners(filepath, stdts):
        """Parses student partners csv
        Args:
            filepath (str): path to student partners csv
            stdts (dict{Student}): dictionary of students, identified by SID

        Returns:
            dict{Student}: dictionary of students, identified by SID
            (modified with partners list)
        """
        # Do nothing if no filepath specified
        if filepath is None:
            return stdts

        with open(filepath, newline='', encoding="utf8") as csvfile:
            csv_reader = csv.reader(csvfile)
            for row in csv_reader:
                for i, stdt_id in enumerate(row):
                    # Add all other sids (except for yourself)
                    stdts[int(stdt_id)].past_partners = row[:i] + row[i+1:]
        return stdts

    def __init__(self, stdt):
        """ Initialize student by parsing row as dict. Does type checking."""
        self.first = stdt['firstname']
        self.last = stdt['lastname']

        if not str.isdigit(stdt['sid']):
            raise Exception(f"SID must be an integer: {stdt['sid']}")
        if int(stdt['sid']) < 0:
            raise Exception(f"SID must be a positive integer: {stdt['sid']}")

        self.sid = int(stdt['sid'])

        # Assume right hand if missing
        try:
            self.left_hand = "l" in stdt['specifiers']
        except IndexError:
            self.left_hand = False

        # Assume no accessible seating need if missing
        try:
            self.ada_seat = "a" in stdt['specifiers']
        except IndexError:
            self.ada_seat = False

        self.past_partners = []

    def __str__(self):
        return json.dumps(self.__dict__)

    def __repr__(self):
        return str(self)

    @staticmethod
    def get_spec_students(stdts, left_hand=False, ada_seat=False):
        """Gets the specified students.
        Args:
            stdts (dict{Student}): dictionary of students, identified by SID
            left_hand (bool): flag for whether to search for left or right
            handed students
            ada_seat (bool): flag for whether to search for accessible seating
            need students

        Returns:
            list[int]: list of specified student ids
        """
        spec_stdts = []
        for stdt in stdts.values():
            if stdt.left_hand == left_hand and stdt.ada_seat == ada_seat:
                spec_stdts.append(stdt.sid)

        return spec_stdts
