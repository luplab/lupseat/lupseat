#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0
#
# Copyright (c) 2020-2022 LupLab et al.
#

import random
import sys
from lupseat.assign import Algorithm
from lupseat.eval import eval_chunk_size
from lupseat.fsave import find_font, save_chart, save_gchart, save_groom
from lupseat.gui import start_gui, done_gui
from lupseat.parser import parse_args
from lupseat.room import Room
from lupseat.student import Student


def main():
    # Setup
    args = parse_args()
    if len(sys.argv) == 1:
        args.gui = True
    if args.gui:
        start_gui(args)
    random.seed(args.seed)

    # Read from files
    stdts = Student.parse_stdt(args.student)
    stdts = Student.parse_stdt_partners(args.partner, stdts)
    room = Room.create_room(args.seats)

    # Assign seats
    alg = Algorithm.choose_algorithm(args.algorithm)
    alg.assign_empty_seats(room, stdts)
    alg.iterative_assign_seats_rand(room, stdts)

    if args.eval:
        # Eval
        eval_chunk_size(room)

    # Set font to use
    args.font = find_font(args.font)

    if not args.nosave:
        # Save output
        if args.nochart is False:
            save_chart(room, args, stdts)
        if args.nopdf is False:
            save_gchart(room, args, stdts)
        if args.noimage is False:
            save_groom(room, args)

    if args.gui:
        done_gui()


if __name__ == "__main__":
    main()
